#!/usr/bin/env bash

. ../.././env
. ./../env

if [ "${CONTAINER_ENGINE}" == "podman" ] ; then
    podman exec -it ${STACK}-${APP}-mariadb /bin/sh
fi

if [ "${CONTAINER_ENGINE}" == "docker" ] ; then
    docker exec -it ${STACK}-${APP}-mariadb /bin/sh
fi
